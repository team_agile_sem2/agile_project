import java.util.List;
import java.util.Scanner;

public class MainInterface 
{	
	databaseConnection database;
	
	public MainInterface(databaseConnection dao) 
	{
		this.database = dao;
	}
	
	public static void main(String[]args) throws addCustomerExceptionHandler, EmployeeExceptionHandler, addPublicationExceptionHandler
	{	
		new agileGUI().Init();
	}
}