import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class publicationController 
{

	private Scanner sc = new Scanner(System.in);
	databaseConnection dbc = new databaseConnection();
	
	public publicationController()
	{
		
	}
	
	public boolean addPublication(publication pub) throws addPublicationExceptionHandler
	{
		try
		{
			//publication
			pub.ValidatePublicationName(pub.getPublicationName());
			
			//publisher name
			pub.ValidatePublisherName(pub.getPublisherName());
			
			//price
			pub.ValidatePrice(pub.getPrice());
			
			//amount
			pub.ValidateAmount(pub.getAmount());
			
			dbc.addPublication(pub);
		}
		catch(addPublicationExceptionHandler e)
		{
			System.out.println(e);
			return false;
		}
		System.out.println("Publication successfully Added");
		return true;
	}
	public boolean changePublication(publication pub) throws addPublicationExceptionHandler
	{
		
		dbc.init_db();
		
		try
		{	
			pub.ValidatePub_id(pub.getPub_id());
			pub.ValidateAmount(pub.getAmount());
		}
		catch(addPublicationExceptionHandler e)
		{
			System.out.println(e);
			return false;
		}
	if(dbc != null)
	{
		ArrayList<Integer> pubID = dbc.getPublicationID();
		
		if(!pubID.isEmpty())
		{
			for(int i = 0; i < pubID.size(); i++)
			{
				if(pub.getPub_id() == pubID.get(i))
				{
					dbc.UpdatePublicationAmount(pub);
					System.out.println("Publication has been Updated");
					return true;
				}
			}
		}
	}
	if(dbc == null)
	{
		return true;
	}
		dbc.cleanup_resources();
		return false;
	}
	public boolean deletePublication(publication pub) throws addPublicationExceptionHandler
	{
		dbc.init_db();
		
		try
		{
			pub.ValidatePub_id(pub.getPub_id());	
		}
		catch(addPublicationExceptionHandler e)
		{
			System.out.println(e);
			return false;
		}
		if(dbc != null)
		{
			ArrayList<Integer> pubID = dbc.getPublicationID();
			
			if(!pubID.isEmpty())
			{
				for(int i = 0; i < pubID.size(); i++)
				{
					if(pub.getPub_id() == pubID.get(i))
					{
						dbc.deletePublication(pub);
						System.out.println("Publication has been Deleted");
						return true;
					}
				}
			}
		}
		if(dbc == null)
		{
			return true;
		}
		
		dbc.cleanup_resources();
		return false;
	}
}
