
public class publication 
{
	private int pub_id;
	private String publisherName;
	private String publicationName;
	private int amount;
	private float price;
	
	//constructor
	public publication()
	{
		
	}
	
	//overloaded constructor
	public publication(int pub_id, String publisherName, String publicationName, Float price, int amount) 
	{
		this.pub_id = pub_id;
		this.publisherName = publisherName;
		this.publicationName = publicationName;
		this.amount = amount;
		this.price = price;
	}
	
	//getters
	public int getPub_id()
	{
		return pub_id;
	}	
	public String getPublisherName()
	{
		return publisherName;
	}	
	public String getPublicationName()
	{
		return publicationName;
	}
	public int getAmount()
	{
		return amount;
	}
	public float getPrice()
	{
		return price;
	}
	
	//setters
	public void setPub_id(int pub_id) 
	{
		this.pub_id = pub_id;
	}
	public void setPublisherName(String publisherName) 
	{
		this.publisherName = publisherName;
	}

	public void setPublicationName(String publicationName) 
	{
		this.publicationName = publicationName;
	}

	public void setAmount(int amount) 
	{
		this.amount = amount;
	}

	public void setPrice(float price) 
	{
		this.price = price;
	}
	
	//pub_id
	public int ValidatePub_id(int pub_id) throws addPublicationExceptionHandler
	{
		this.pub_id = pub_id;
		
		if(pub_id >= 1)
		{
			return pub_id;
		}
		else
		{
			throw new addPublicationExceptionHandler("Invalid Publication ID....Must be more or quals 1");
		}
	}
	
	//publisher name
	public boolean ValidatePublisherName(String PublisherName) throws addPublicationExceptionHandler
	{
		this.publisherName = PublisherName;

		if(publisherName.length() > 0 && publisherName.length() <=20)
		{
			return true;
		}
		else
		{
			throw new addPublicationExceptionHandler("Invalid publisher Name... Name has to contains up to 20 characters and contain at least 1 character");
		}
	}
	
	//publication name
	public boolean ValidatePublicationName(String PublicationName) throws addPublicationExceptionHandler
	{
		this.publicationName = PublicationName;
		
		if(publicationName.length() > 0 && publicationName.length() <=20)
		{
			return true;
		}
		else
		{
			throw new addPublicationExceptionHandler("Invalid Publication Name... Needs to contains up to 20 characters and at least 1 character");
		}
	}
	
	//Amount
	public int ValidateAmount(int Amount) throws addPublicationExceptionHandler
	{
		this.amount = Amount;
		
		if(amount > 0)
		{
			return amount;
		}
		else
		{
			throw new addPublicationExceptionHandler("Amount needs to be more than 0");
		}
	}
	//Price
	public float ValidatePrice(float Price) throws addPublicationExceptionHandler
	{	
		this.price = Price;
		
		if(price > 0.00)
		{
			return price;
		}
		else
		{
			throw new addPublicationExceptionHandler("Price needs to be more than 0");
		}
	}
	public String toString()
	{
		return "Pub_ID: " + pub_id + "\n" + "Publisher Name: " + publisherName + "\n" + "Publication Name: " + publicationName + "\n" + "Price: " + price + "\n" + "Amount: " + amount + "\n";
	}
}
