import java.util.ArrayList;

import junit.framework.TestCase;

public class DeliveriesControllerTest extends TestCase 
{
	//Test No:
	//Test Objective:
	//Input(s):
	//Expected Output:

	//Test No:001	
	//Test Objective:test that the list in not null
	//Input(s): databaseConnection object
	//Expected Output: true
	
	public void testList001()
	{
		DeliveriesController del = new DeliveriesController();
		databaseConnection data = new databaseConnection();
		
		assertEquals(del.checkList(del.getDeliveries(data)),true);
	}
	
	//Test No:002	
	//Test Objective:test that the list is null
	//Input(s): databaseConnection object
	//Expected Output: false

	public void testList002()
	{
		DeliveriesController del = new DeliveriesController();
		databaseConnection data = new databaseConnection();
		
		ArrayList<Deliveries> d = new ArrayList<Deliveries>();
		assertEquals(del.checkList(d),false);
	}
}
