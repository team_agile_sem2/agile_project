public class DeliContent 
{
	private int deliID;
	private int pubID;
	
	public DeliContent(int deliID, int pubID)
	{
		this.deliID = deliID;
		this.pubID = pubID;
	}
	
	public int getdeliID()
	{
		return deliID;
	}
	
	public int getpubID()
	{
		return pubID;
	}
}
