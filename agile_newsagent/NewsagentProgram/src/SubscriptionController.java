import java.sql.SQLException;

public class SubscriptionController {
	private databaseConnection dbc;
	
	public SubscriptionController(databaseConnection dao)
	{
		this.dbc = dao;
	}
	
	public boolean addSubscription(Customer cus, publication pub, Subscription sub) throws addCustomerExceptionHandler, addPublicationExceptionHandler, SQLException
	{
		dbc.init_db();
		boolean done = false;
		try
		{
			cus.ValidateCusId(cus.getCusId());
			pub.ValidatePub_id(pub.getPub_id());
			done = dbc.addSubscription(cus, pub, sub);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		dbc.cleanup_resources();
		
		if(!done && dbc != null)
		{
			return false;
		}
		
		System.out.println("Subscription Succesfully Added");
		return true;
	}
}
