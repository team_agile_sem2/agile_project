
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CustomerController 
{
	private Scanner sc = new Scanner(System.in);
	databaseConnection dbc;
	
	public CustomerController(databaseConnection dao)
	{
		this.dbc = dao;
	}
	
	
	public boolean addCustomer(Customer cus) throws addCustomerExceptionHandler
	{
		dbc.init_db();
		try
		{	
			
			cus.ValidateFirstName(cus.getFirstName());
			
			cus.ValidateLastName(cus.getLastName());
			
			cus.ValidateAge(cus.getAge());
			
			cus.ValidateAddress1(cus.getAddress1());
			
			cus.ValidateAddress2(cus.getAddress2());
			
			cus.ValidateTown(cus.getTown());
			
			cus.ValidateCounty(cus.getCounty());
			
			cus.ValidatePhoneNumber(cus.getPhoneNumber());
			
			cus.getCustomerStatus();
			
			dbc.addCustomer(cus);
		}
		catch(addCustomerExceptionHandler e)
		{
			System.out.println(e);
			return false;
		}
		
		System.out.println("Customer successfully Added");
		dbc.cleanup_resources();
		return true;
	}
	
	public boolean changeCustomerAddress(Customer cus) throws addCustomerExceptionHandler
	{
		boolean failed = true;
		
		dbc.init_db();
		try
		{
			cus.ValidateCusId(cus.getCusId());

			cus.ValidateAddress1(cus.getAddress1());
			
			cus.ValidateAddress2(cus.getAddress2());
			
			cus.ValidateTown(cus.getTown());

			cus.ValidateCounty(cus.getCounty());
		
			dbc.UpdateCustomerAddress(cus);
		}
		catch(addCustomerExceptionHandler e)
		{
			System.out.println(e);
			return false;
		}
		
		if(dbc != null)
		{
			ArrayList<Integer> cusList = dbc.getCustomerID();
			for (int i: cusList)
			if(cusList.contains(cus.getCusId()))
			{
				failed = true;
				break;
			}
			else
			{
				failed = false;
			}
		}
		
		if(!failed)
		{
			System.out.println("The CusID doesn't exist!");
			return false;
		}
		
		System.out.println("Customer Succesfully Changed");
		System.out.println("  ");
		dbc.cleanup_resources();
		return true;
	}
	
	public boolean changeCustomerPhoneNumber(Customer cus) throws addCustomerExceptionHandler
	{
		boolean failed = true;
		dbc.init_db();
		try
		{
			
			cus.ValidateCusId(cus.getCusId());

			cus.ValidatePhoneNumber(cus.getPhoneNumber());
			
			dbc.UpdateCustomerPhoneNumber(cus);
			
		}
		catch(addCustomerExceptionHandler e)
		{
			System.out.println(e);
			return false;
		}
		if(dbc != null)
		{
			ArrayList<Integer> cusList = dbc.getCustomerID();
			for (int i: cusList)
			if(cusList.contains(cus.getCusId()))
			{
				failed = true;
				break;
			}
			else
			{
				failed = false;
			}
		}
		
		if(!failed)
		{
			System.out.println("The CusID doesn't exist!");
			return false;
		}
		
		System.out.println("Phone Number Succesfully Changed!");
		dbc.cleanup_resources();
		return true;
	}
	
	public boolean changeCustomerStatus(Customer cus) throws addCustomerExceptionHandler
	{
		boolean failed = true;
		
		dbc.init_db();
		try
		{
			
			cus.ValidateCusId(cus.getCusId());
			cus.getCustomerStatus();
			dbc.updateCustomerStatus(cus);
		}
		catch(addCustomerExceptionHandler e)
		{
			System.out.println(e);
			return false;
		}
		
		if(dbc != null)
		{
			ArrayList<Integer> cusList = dbc.getCustomerID();
			for (int i: cusList)
			if(cusList.contains(cus.getCusId()))
			{
				failed = true;
				break;
			}
			else
			{
				failed = false;
			}
		}
		
		if(!failed)
		{
			System.out.println("The CusID doesn't exist!");
			return false;
		}
		
		if(!failed)
		{
			System.out.println("The CusID doesn't exist!");
			return false;
		}
		
		dbc.cleanup_resources();
		System.out.println("Status Succesfully Updated!");
		return true;
	}
	
	public boolean deleteCustomer(Customer cus) throws addCustomerExceptionHandler
	{
		boolean failed = true;
		
		dbc.init_db();
	
		try
		{
			cus.ValidateCusId(cus.getCusId());	
		}
		catch(addCustomerExceptionHandler e)
		{
			System.out.println(e);
			return false;
		}
		
		if(dbc != null)
		{
			ArrayList<Integer> cusList = dbc.getCustomerID();
			for (int i: cusList)
			if(cusList.contains(cus.getCusId()))
			{
				failed = true;
				dbc.DeleteCustomer(cus);
				break;
			}
			else
			{
				failed = false;
			}
		}
		
		if(!failed)
		{
			System.out.println("The CusID doesn't exist!");
			return false;
		}
		
		dbc.cleanup_resources();
		System.out.println("Customer deleted Sucesfully!");
		return true;
	}
}