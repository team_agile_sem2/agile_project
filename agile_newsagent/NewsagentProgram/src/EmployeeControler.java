import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EmployeeControler extends Employee
{
		 	databaseConnection dbc = new databaseConnection();
			private int EmpId;
		 	
		 	
		 	public EmployeeControler(databaseConnection dao)
		 	{
		 		this.dbc = dao;
		 	}
		 	
		 	public void printEmployees()
			{
				List<Employee> empList = this.dbc.getEmployee();
				for (Employee emp: empList)
					emp.toString();
			}
		 	
		 	public boolean addEmployee(Employee emp2) throws EmployeeExceptionHandler
		 	{
		 		Employee emp = emp2;
		 		try
		 		{
		 			
		 				emp.ValidateFirstName(emp2.getFirst_name());
		 		
		 				emp.ValidateLastName(emp2.getLast_name());
						
			 			emp.ValidateAddress(emp2.getAddress());
			 			
						emp.ValidatePhoneNumber(emp2.getPhone_no());
						
						emp.ValidateAge(emp2.getAge());
			 
					databaseConnection.addEmployee(emp);
		 		}		 			
		 	
		 		catch(EmployeeExceptionHandler e)
		 		{
		 			System.out.println(e);
		 			return false;
		 		}
				return true;
		 	}		
		 	
		 	public boolean deleteEmployee(Employee emp) throws EmployeeExceptionHandler
			{
		 		boolean failed = true;
				
				databaseConnection.init_db();
			
				try
				{
					emp.ValidateEmpId(emp.getEmpId());	
				}
				catch(EmployeeExceptionHandler e)
				{
					System.out.println(e);
					return false;
				}
				
				if(dbc != null)
				{
					ArrayList<Integer> empList = dbc.getEmployeeID();
					for (int i: empList)
					if(empList.contains(emp.getEmpId()))
					{
						failed = true;
						databaseConnection.DeleteEmployee(emp);
						break;
					}
					else
					{
						failed = false;
					}
				}
				
				if(!failed)
				{
					System.out.println("The empID doesn't exist!");
					return false;
				}
				
				databaseConnection.cleanup_resources();
				System.out.println("Employee deleted Sucesfully!");
				return true;
			}
		 	public boolean changeEmployeeAddress(Employee emp) throws EmployeeExceptionHandler
			{
				
				printEmployees();
				
				try
				{
					emp.ValidateEmpId(emp.getEmpId());
					emp.ValidateAddress(emp.getAddress());
				
					databaseConnection.UpdateEmployeeAddress(emp);
				}
				catch(EmployeeExceptionHandler e)
				{
					System.out.println(e);
					return false;
				}
				
				printEmployees();
				System.out.println("Employee Succesfully Changed");
				System.out.println("  ");
				return true;
			}
			
			public boolean changeEmployeePhoneNumber(Employee emp) throws EmployeeExceptionHandler
			{
				
				printEmployees();
				
				try
				{
					
					emp.ValidateEmpId(emp.getEmpId());

					emp.ValidatePhoneNumber(emp.getPhone_no());
					
					databaseConnection.UpdateEmployeePhoneNumber(emp);
				}
				catch(EmployeeExceptionHandler e)
				{
					System.out.println(e);
					return false;
				}
				
				printEmployees();
				return true;
			}
}

