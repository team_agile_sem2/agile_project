import junit.framework.TestCase;

public class EmployeeControllerTest extends TestCase {

				//Test No.1
				//Test Objective:invalid if name is greater than 16
				//Input(s): first_name = "abcdghlkjhgfdasgy"
				//Expected Output:Exception message Expected "Invalid first name"
					
			public void testAddingEmployee0001() {
					
				Employee testObject = new Employee();
					
					try {
						
						testObject.ValidateFirstName("abcdghlkjhgfdasgy");
						
					}
					catch (EmployeeExceptionHandler e) {
						
						assertEquals("Invalid first name", e.getMessage());
					}
				}
			
			//Test No.2
			//Test Objective:valid first name
			//Input(s): first_name = "Darragh"
			//Expected Output:"Darragh"
				
		public void testAddingEmployee0002() {
				
			Employee testObject = new Employee();
				
				try {
					
					testObject.ValidateFirstName("Darragh");
					
				}
				catch (EmployeeExceptionHandler e) {
					fail("Should not get here .. no Exception Expected");
				}
			}
		
			//Test No.3
			//Test Objective:invalid if name is empty
			//Input(s): first_name = " "
			//Expected Output:Exception message Expected "Invalid first name"
			
		public void testAddingEmployee0003() {
				
			Employee testObject = new Employee();
				
				try {
					
					testObject.ValidateFirstName(" ");
					
				}
				catch (EmployeeExceptionHandler e) {
					
					assertEquals("Invalid first name", e.getMessage());
				}
			}
			//Test No.4
			//Test Objective:invalid if last name is greater than 16
			//Input(s): last_name = "abcdghlkjhgfdasgy"
			//Expected Output:Exception message Expected "Invalid last name"
				
		public void testAddingEmployee0004() {
				
			Employee testObject = new Employee();
				
				try {
					
					 testObject.ValidateLastName("abcdghlkjhgfdasgy");
					
				}
				catch (EmployeeExceptionHandler e) {
					
					assertEquals("Invalid last name", e.getMessage());
				}
			}
		
			//Test No.5
			//Test Objective:valid last name
			//Input(s): last_name = "Clerkin"
			//Expected Output:"Clerkin"
			
		public void testAddingEmployee0005() {
			
			Employee testObject = new Employee();
			
			try {
				
				 testObject.ValidateLastName("Clerkin");
				
			}
			catch (EmployeeExceptionHandler e) {
				fail("Should not get here .. no Exception Expected");
			}
		}
		
			//Test No.6
			//Test Objective:invalid if last name is empty
			//Input(s): last_name = " "
			//Expected Output:Exception message Expected "Invalid last name"
		
		public void testAddingEmployee0006() {
			
			Employee testObject = new Employee();
			
			try {
				
				 testObject.ValidateLastName(" ");
				
			}
			catch (EmployeeExceptionHandler e) {
				
				assertEquals("Invalid last name", e.getMessage());
			}
		}
			//Test No.7
			//Test Objective:invalid address is greater than 16
			//Input(s): address = "abcdghlkjhgfdasgy"
			//Expected Output:Exception message Expected "Invalid address"
					
			public void testAddingEmployee0007() {
					
				Employee testObject = new Employee();
					
					try {
						
						 testObject.ValidateAddress("abcdghlkjhgfdasgy");
						
					}
					catch (EmployeeExceptionHandler e) {
						
						assertEquals("Invalid address", e.getMessage());
					}
				}
			
				//Test No.8
				//Test Objective:valid address
				//Input(s): address = "Monaghan"
				//Expected Output:"Monaghan"
				
			public void testAddingEmployee0008() {
				
				Employee testObject = new Employee();
				
				try {
					
					 testObject.ValidateAddress("Monaghan");
					
				}
				catch (EmployeeExceptionHandler e) {
					fail("Should not get here .. no Exception Expected");
				}
			}
			
				//Test No.9
				//Test Objective:invalid address is empty
				//Input(s): address = " "
				//Expected Output:Exception message Expected "Invalid, address"
			
			public void testAddingEmployee0009() {
				
				Employee testObject = new Employee();
				
				try {
					
					 testObject.ValidateAddress(" ");
					
				}
				catch (EmployeeExceptionHandler e) {
					
					assertEquals("Invalid address", e.getMessage());
				}
			}
			
					
						//Test No.10
						//Test Objective:valid age
						//Input(s): age = "18"
						//Expected Output:18
						
					public void testAddingEmployee00010() {
						
						Employee testObject = new Employee();
						
						try {
							
							 testObject.ValidateAge(18);
							
						}
						catch (EmployeeExceptionHandler e) {
							fail("Should not get here .. no Exception Expected");
						}
					}
					
						//Test No.11
						//Test Objective:invalid age is empty
						//Input(s): address = " "
						//Expected Output:Exception message Expected "Invalid age"
					
					public void testAddingEmployee00011() {
						
						Employee testObject = new Employee();
						
						try {
							
							 testObject.ValidateAge(0);
							
						}
						catch (EmployeeExceptionHandler e) {
							
							assertEquals("Invalid age", e.getMessage());
						}
					}
					//Test No.12
					//Test Objective:invalid age 10
					//Input(s): age = "100"
					//Expected Output:Exception message Expected "Invalid age"
							
					public void testAddingEmployee00012() {
							
						Employee testObject = new Employee();
							
							try {
								
								 testObject.ValidateAge(100);
								
							}
							catch (EmployeeExceptionHandler e) {
								
								assertEquals("Invalid age", e.getMessage());
							}
						}
					
						//Test No.13
						//Test Objective:valid Phone number
						//Input(s): phone_no = "08645897"
						//Expected Output:"08645897"
						
					public void testAddingEmployee00013() {
						
						Employee testObject = new Employee();
						
						try {
							
							assertEquals(true,testObject.ValidatePhoneNumber("08645897"));
							
						}
						catch (EmployeeExceptionHandler e) {
							fail("Should not get here .. no Exception Expected");
						}
					}
					
				//Test No.14
				//Test Objective:invalid phone number is empty
				//Input(s): phone_no = " "
				//Expected Output:Exception message Expected "Invalid Phone number"
					
					public void testAddingEmployee00014() {					
						Employee testObject = new Employee();
						
						try {
							
							 testObject.ValidatePhoneNumber(" ");
							
						}
						catch (EmployeeExceptionHandler e) {
							
							assertEquals("Invalid Phone number", e.getMessage());
						}
					}
	}


