import junit.framework.TestCase;

public class SubscriptionControllerTest extends TestCase 
{
	//Equivelance Partition for Adding Subscription
	//Test No:01
	//Test Objective: Valid Subscription Adding
    //Input(s):daily, 1000, 1
    //Expected Output: true  
    
    public void testSubscription01() throws Exception
    {
  	  	SubscriptionController testObject = new SubscriptionController(null);
  	  	
        Subscription sub = new Subscription();
        sub.setFrequency("daily");
        
        Customer cus = new Customer();
        cus.setCusId(1000);
        
        publication pub = new publication();
        pub.setPub_id(1);
        
        try
        {
      	  assertEquals(true, testObject.addSubscription(cus, pub, sub));
        }
        catch(Exception e)
        {
      	  fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:02
    //Test Objective: Invalid Subscription Adding
    //Input(s): dailyy, 23, 1
    //Expected Output: false  
    
    public void testSubscription02() throws Exception
    {
    	SubscriptionController testObject = new SubscriptionController(null);
  	  	
        Subscription sub = new Subscription();
        sub.setFrequency("dailyy");
        
        Customer cus = new Customer();
        cus.setCusId(23);
        
        publication pub = new publication();
        pub.setPub_id(1);
        
        try
        {
      	  assertEquals(false, testObject.addSubscription(cus, pub, sub));
        }
        catch(Exception e)
        {
      	  fail("Should not get here ... Exception is expected");
        }
    }
}
