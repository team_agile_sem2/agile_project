
public class Customer
{
	//Attributes
	private int cusId;
	private String firstName;
	private String lastName;
	private String address1;
	private String address2;
	private String town;
	private String county;
	private int age;
	private String cus_status;
	
	public Customer()
	{
		
	}
	
	public Customer(int cusId, String firstName, String lastName, String address1, String address2, String town, String county, int age, String phoneNumber, String customerStatus) 
	{
		this.cusId = cusId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address1 = address1;
		this.address2 = address2;
		this.town = town;
		this.county = county;
		this.age = age;
		this.phoneNumber = phoneNumber;
		this.cus_status = customerStatus;
	}

	private String phoneNumber;
	
	public int getCusId() {
		return cusId;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public int getAge() {
		return age;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getAddress1() {
		return address1;
	}
	
	public String getAddress2() {
		return address2;
	}
	
	public String getTown() {
		return town;
	}
	
	public String getCounty() {
		return county;
	}
	
	public String getCustomerStatus() {
		return cus_status;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setCusId(int cusId) {
		this.cusId = cusId;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	public void setCustomerStatus(String cusStatus) {
		this.cus_status = cusStatus;
	}

	//Method
	//Validates firstName
	public boolean ValidateFirstName(String FirstName) throws addCustomerExceptionHandler
	{
		this.firstName = FirstName;
		
		if(firstName.matches( "[A-Z][a-zA-Z]*" ) && firstName.length() > 0 && firstName.length() <=16)
		{
			return true;
		}
		else
		{
			throw new addCustomerExceptionHandler("Invalid First Name must Start with capital and can only contains up to 16 characters");
		}
	}
	
	//Validates lastName
	public boolean ValidateLastName(String LastName) throws addCustomerExceptionHandler
	{
		this.lastName = LastName;
		
		if(lastName.matches( "[a-zA-z]+([ '-][a-zA-Z]+)*" )&& lastName.length() > 0 && lastName.length() <=16)
		{
			return true;
		}
		else
		{
			throw new addCustomerExceptionHandler("Invalid Last Name/ can't be less 1 and greter than 16");
		}
	}
	
	//Validates Age
	public int ValidateAge(int Age) throws addCustomerExceptionHandler
	{
		this.age = Age;
		
		if(age >= 18 && age <= 100)
		{
			return age;
		}
		else
		{
			throw new addCustomerExceptionHandler("Invalid Age must be between 18 and 100");
		}
	}
	
	//Validates cusId
		public int ValidateCusId(int cusId) throws addCustomerExceptionHandler
		{
			this.cusId = cusId;
			
			if(cusId >= 1000)
			{
				return cusId;
			}
			else
			{
				throw new addCustomerExceptionHandler("Invalid Customer ID it must start from 1000");
			}
		}
	
	//Validates PhoneNumber
	public boolean ValidatePhoneNumber(String Phone) throws addCustomerExceptionHandler
	{
		this.phoneNumber = Phone;
		
		if(phoneNumber.matches( "[0-9]*" )&& phoneNumber.length() == 10)
		{
			return true;
		}
		else
		{
			throw new addCustomerExceptionHandler("Invalid phone number must start with 0 and have 10 characters");
		}
	}
	
	//Validates Address
	public boolean ValidateAddress1(String add) throws addCustomerExceptionHandler
	{
		this.address1 = add;
		
		if(address1.length() > 0 && address1.length() <=20)
		{
			return true;
		}
		else
		{
			throw new addCustomerExceptionHandler("Invalid Address1 must be between 1 and 20 characters");
		}
	}
	
	public boolean ValidateAddress2(String add) throws addCustomerExceptionHandler
	{
		this.address2 = add;
		
		if(address2.length() > 0 && address2.length() <=20)
		{
			return true;
		}
		else
		{
			throw new addCustomerExceptionHandler("Invalid Address2 must be between 1 and 20 characters");
		}
	}
	
	public boolean ValidateTown(String add) throws addCustomerExceptionHandler
	{
		this.town = add;
		
		if(town.matches( "[A-Z][a-zA-Z]*" ) && town.length() > 0 && town.length() <=16)
		{
			return true;
		}
		else
		{
			throw new addCustomerExceptionHandler("Invalid Town must start with capital and contain more than 1 and less than 17 characters");
		}
	}
	
	public boolean ValidateCounty(String add) throws addCustomerExceptionHandler
	{
		this.county = add;
		
		if(county.matches( "[A-Z][a-zA-Z]*" ) && county.length() > 0 && county.length() <=16)
		{
			return true;
		}
		else
		{
			throw new addCustomerExceptionHandler("Invalid County must start with capital and contain more than 1 and less than 17 characters");
		}
	}
	
	public String toString()
	{
		return "cus_id: " + cusId + "\n" + "Name: " + firstName + " " + "\nLastName: " + lastName + "\n" + "Age: " + age + " \n" + "Address1: " + address1 +  " " + "\nAddress2: " + address2 + " "  + "\nTown: " + town + " " + "\nCounty: " + county + "\n" + "PhoneNumber: " + phoneNumber + "\n" + "Status: " + cus_status + "\n";
	}
}
