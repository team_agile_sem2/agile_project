import junit.framework.TestCase;

public class AccountTest extends TestCase 
{
	//Test No:
	//Test Objective:
	//Input(s):
	//Expected Output:
	
	/*testing the length of both password and username
	 * using Boundary value analysis
	 * length must be at least 6 and a maximum of 12 characters
	*/
	
	//Test No:001
	//Test Objective: Invalid password and username length
	//Input(s): userName = newsA password = 5qter
	//Expected Output: Invalid username/password length! \n Must be atleast 6 and a maximum of 12 characters!
	public void testLength001()
	{
		AccountController testObject = new AccountController();
		try
		{
			testObject.validateLength("newsA","5qter");
			
			fail("Exception was expected");
		}
		catch(IllegalArgumentException e)
		{
			assertEquals("Invalid username/password length! \n Must be atleast 6 and a maximum of 12 characters!\n",e.getMessage());
		}
	}

	//Test No:002
	//Test Objective: valid password and username length
	//Input(s): userName = newsAg password = 5qterD
	//Expected Output: true
	public void testLength002()
	{
		AccountController testObject = new AccountController();
		
		assertEquals(testObject.validateLength("newsAg","5qterD" ), true);
	}

	//Test No:003
	//Test Objective: valid password and username length
	//Input(s): userName = newsAge password = 5qterD5
	//Expected Output: true
	public void testLength003()
	{
		AccountController testObject = new AccountController();
		
		assertEquals(testObject.validateLength("newsAge","5qterD5" ), true);
	}

	//Test No:004
	//Test Objective: valid password and username length
	//Input(s): userName = newsAgent01 password = 5qterDeywue
	//Expected Output: true
	public void testLength004()
	{
		AccountController testObject = new AccountController();
		
		assertEquals(testObject.validateLength("newsAgent01","5qterDeywue" ), true);
	}
	
	//Test No:005
	//Test Objective: valid password and username length
	//Input(s): userName = newsAgent password = 5qterDeywued
	//Expected Output: true
	public void testLength005()
	{
		AccountController testObject = new AccountController();
		
		assertEquals(testObject.validateLength("newsAgent001","5qterDeywued" ), true);
	}

	//Test No:006
	//Test Objective: Invalid password and username  length
	//Input(s): userName = newsAgent password = 5qterDeywueda
	//Expected Output: Invalid username/password length! \n Must be atleast 6 and a maximum of 12 characters!
	public void testLength006()
	{
		AccountController testObject = new AccountController();
		try
		{
			testObject.validateLength("newsAgent0012","5qterDeywueda" );
			
			fail("Exception was expected");
		}
		catch(IllegalArgumentException e)
		{
			assertEquals("Invalid username/password length! \n Must be atleast 6 and a maximum of 12 characters!\n", e.getMessage());
		}
	}

	
	/*testing the password if it contains atleast 
	 * one uppercase and one number
	 * Using eqivalence partitioning
	 */
	
	//Test No:007
	//Test Objective: invalid password when it has no number characters
	//Input(s): "Qterdeywue"
	//Expected Output: false
	public void testPassword001()
	{
		AccountController testObject = new AccountController();
		try
		{
			testObject.validatePassword("Qterdeywue");
			fail("Exception was expected");
		}
		catch(IllegalArgumentException e)
		{
			assertEquals("Invalid password! \nMust have atleast have a uppercase and number!\n",e.getMessage());
		}
	}
	
	//Test No:008
	//Test Objective: invalid password it has is no uppercase characters
	//Input(s):"4terdeywue"
	//Expected Output: false
	public void testPassword002()
	{
		AccountController testObject = new AccountController();
		
		try
		{
			testObject.validatePassword("4terdeywue");
			fail("Exception was expected");
		}
		catch(IllegalArgumentException e)
		{
			assertEquals("Invalid password! \nMust have atleast have a uppercase and number!\n",e.getMessage());
		}
	}
	
	//Test No:009
	//Test Objective: invalid password when it has no uppercase and number characters
	//Input(s):"qterdeywue"
	//Expected Output: false
	public void testPassword003()
	{
		AccountController testObject = new AccountController();
		
		try
		{
			testObject.validatePassword("qterdeywue");
			fail("Exception was expected");
		}
		catch(IllegalArgumentException e)
		{
			assertEquals("Invalid password! \nMust have atleast have a uppercase and number!\n",e.getMessage());
		}
	}
	
	//Test No:010
	//Test Objective: valid password where it has both uppercase and number characters
	//Input(s):"Qter5eywue"
	//Expected Output: true
	public void testPassword004()
	{
		AccountController testObject = new AccountController();
		
		try
		{
			assertEquals(testObject.validatePassword("Qter5eywue"),true);
		}
		catch(IllegalArgumentException e)
		{
			e.getMessage();
			fail("Exception was not expected");
		}
	}
	
	
	/*Log_in 
	 * Testing for that username and password match 
	 * using equivalence partioning
	 */
	
	
	//Test No:011
	//Test Objective: verify that username and password match the log details in the data base is valid
	//Input(s):"Qter5eywue"
	//Expected Output: true;
	public void testlogIn001()
	{
		AccountController testObject = new AccountController();
		
		assertEquals(true,testObject.logIn("marku", "nyeahs"));
	}
	
	//Test No:012
	//Test Objective: username matches and 
	//password does not match info in database results into a invalid output
	//Input(s):"Qter5eywue"
	//Expected Output: false;
	public void testlogIn002()
	{
		AccountController testObject = new AccountController();
		
		assertEquals(testObject.logIn("marku", "asdqwe"), false);
	}
	
	//Test No:013
	//Test Objective: password matches and 
	//username does not match info in database results into a invalid output
	//Input(s):"Qter5eywue"
	//Expected Output: false;
	public void testlogIn003()
	{
		AccountController testObject = new AccountController();
		
		assertEquals(testObject.logIn("markw", "nyeahs"),false);
	}
	
	//Test No:014
	//Test Objective: both username and 
	//password does not match info in database results into a invalid output
	//Input(s):"Qter5eywue"
	//Expected Output: false;
	public void testlogIn004()
	{
		AccountController testObject = new AccountController();
		
		assertEquals(testObject.logIn("maqwe", "asdqwe"),false);
	}

}
