public class Deliveries
{
	private int deliId;
	private int empId;
	private int cusId;
	private String address;
	private String date;
	private String pubName;
	
	
	public Deliveries()
	{
		this.deliId = 0;
		this.empId = 0;
		this.cusId = 0;
		this.date = null;
		this.pubName = null;
		this.address = null;
	}
	public void addDeliveries(int deliId,int empId,int cusId,String address, String date)
	{
		this.deliId = deliId;
		this.empId = empId;
		this.cusId = cusId;
		this.date = date;
		this.address = address;
	}
	
	public void setPubName(String pubName)
	{
		this.pubName = pubName;
	}
	
	public void setAddress(String address)
	{
		this.address = address;
	}
	
	public int getdeliID()
	{
		return deliId;
	}
	public int getempID()
	{
		return empId;
	}
	public int getcusID()
	{
		return cusId;
	}
	public String getDate()
	{
		return date;
	}
	public String toString()
 	{
		return "deli_id: "+ deliId + "\n"+"emp_id: " + empId  + "\n" + "cus_id: " + cusId +"\n" + "Address: " + address + "\n" + "Date: " + date + "\n" + "Publication: " + pubName;
 	}
}
