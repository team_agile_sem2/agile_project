public class Account 
{
	private String username;
	private String password;
	
	public Account(String username, String password)
	{
		this.username = username;
		this.password = password;
	}
	
	public String getUser()
	{
		return this.username;
	}
	
	public String getPass()
	{
		return this.password;
	}

}
