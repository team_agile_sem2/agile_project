import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AccountController 
{
	
	databaseConnection dbc = new databaseConnection();
	
	public boolean validateLength(String userName, String password)
	{
		if((userName.length() <= 12 && userName.length() >= 6) && (password.length() <= 12 && password.length() >= 6))
		{
			return true;
		}
		else
		{
			throw new IllegalArgumentException("Invalid username/password length! \n Must be atleast 6 and a maximum of 12 characters!\n");
		}
	}
	
	public boolean  validatePassword(String password)
	{
		boolean hasUpper = false;
		boolean hasNumber = false;
		
		for(int i = 0; i < password.length(); i++)
		{
			char l = password.charAt(i);
			
			if(Character.isUpperCase(l))
			{
				hasUpper = true;
			}
			if(Character.isDigit(l))
			{
				hasNumber = true;
			}
		}
		
		if(hasUpper && hasNumber)
		{
			return true;
		}
		else
		{
			throw new IllegalArgumentException("Invalid password! \nMust have atleast have a uppercase and number!\n");
		}
	}
	
	public boolean createAccount(String username, String password) 
	{
		Account acc;
		try
		{
			validateLength(username, password);
			validatePassword(password);
			acc = new Account(username, password);
			System.out.print("");
		}
		catch(IllegalArgumentException e)
		{
			System.out.print(e);
			return false;
		}
		dbc.addAccount(acc);
		System.out.println("account successfully created");
		return true;
	}
	
	
	public boolean logIn(String username, String password)
	{
		dbc.init_db();
		List<String> user = dbc.getUserName();
		List<String> pass = dbc.getPassword();
		for(int x= 0;x < user.size();x++)
		{
			if(username.equals(user.get(x)) && password.equals(pass.get(x)))
			{
				System.out.println("Log in Successful!");
				return true;
			}
		}
		System.out.println("Username/Password did not match!");
		return false;
		
	}
	
}
