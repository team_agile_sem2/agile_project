
public class Employee 
{
	private int empId;
	private String first_name;
	private String last_name;
	private int age;
	private String address;
	private String phone_no;
	
	public Employee()
	{
		
	}
	//Overloaded Constructor
	public Employee(int empId, String first_name, String last_name, String address,int age, String phone_no)
	{
		this.empId = empId;
		this.first_name = first_name;
		this.last_name = last_name;
		this.age = age;
		this.address = address;
		this.phone_no = phone_no;
	}
	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getFirst_name() {
		return first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public int getAge() {
		return age;
	}

	public String getAddress() {
		return address;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	
	//Method for validating first_name
	public boolean ValidateFirstName(String first_name)throws EmployeeExceptionHandler
	{	
		this.first_name = first_name;
		if( (first_name.matches  ("[A-Z][A-Za-z]*")) && (first_name.length()> 0) && (first_name.length() <= 16))
		{
				return true;
		}
		else
		{
			throw new EmployeeExceptionHandler("Invalid first name");
		}	
	}
	
	public boolean ValidateLastName(String last_name)throws EmployeeExceptionHandler
	{	
		this.last_name = last_name;
		if(last_name.matches  ("[A-Z][A-Za-z]*") && (last_name.length() >0) && (last_name.length() <= 16))
		{
				return true;
		}
		else
		{
			throw new EmployeeExceptionHandler("Invalid last name");
		}	
	}
	
	public boolean ValidateAddress(String address)throws EmployeeExceptionHandler
	{	
		this.address = address;
		if(address.matches  ("[A-Z][A-Za-z]*") && address.length()> 0 && address.length() <= 16)
		{
				return true;
		}
		else
		{
			throw new EmployeeExceptionHandler("Invalid address");
		}	
	}
	
	public boolean ValidatePhoneNumber(String phone_no)throws EmployeeExceptionHandler
	{	
		this.phone_no = phone_no;
		if(phone_no.matches  ("[0-9]*") && phone_no.length()>= 0 && phone_no.length() <= 10)
		{
				return true;
		}
		else
		{
			throw new EmployeeExceptionHandler("Invalid Phone number");
		}
	}

	public int ValidateAge(int age)throws EmployeeExceptionHandler
	{
		this.age = age;
		if(age > 0 && age <= 100)
		{
				return age;
		}
		else
		{
			throw new EmployeeExceptionHandler("Invalid age");
		}
	}
	public int ValidateEmpId(int empId) throws EmployeeExceptionHandler
	{
		this.empId = empId;
		
		if(empId >= 1)
		{
			return empId;
		}
		else
		{
			throw new EmployeeExceptionHandler("Invalid Employee ID it must be greater that or equal to 1");
		}
	}
	public String toString()
 	{
		return "emp_id: "+ empId + "\n"+"Name: " + first_name + "\n" + "LastName: " + last_name + "\n" + "Address: " + address + "\n" + "Phone Number: " + phone_no + "\n" + "Age: " + age;
 	}
}