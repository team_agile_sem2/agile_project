import java.util.ArrayList;
import java.util.List;

public class DeliveriesController 
{
	
	public ArrayList<Deliveries> getDeliveries(databaseConnection database)
	{
		database.init_db();
		DeliveriesController check = new DeliveriesController();
		ArrayList<Deliveries> deliverList = new ArrayList<Deliveries>();
		ArrayList<Deliveries> delList = database.getDeliveries();
		List<Customer> cus = new ArrayList<Customer>(database.getCustomers());
		ArrayList<publication> pub = database.getPublications();
		ArrayList<DeliContent> deli = database.getDeliContent();
		if(check.checkList(delList))
		{
			for (Deliveries deliveries: delList)
			{
				for(DeliContent dCont: deli)
				{
					if(deliveries.getdeliID() == dCont.getdeliID())
					{
						int deliID = deliveries.getdeliID();
						int empID = deliveries.getempID();
						int cusID = deliveries.getcusID();
						String date = deliveries.getDate();
						String publicationName = " ";
						String address = " ";
						for(publication publication:pub)
						{
							if(publication.getPub_id() == dCont.getpubID())
							{
								publicationName = publication.getPublicationName();
							}
						}
						for(Customer customer: cus)
						{
							if(cusID == customer.getCusId())
							{
								address = "\n" + customer.getAddress1() + ",\n" + customer.getAddress2() +",\n"+ customer.getTown() + ",\n" + customer.getCounty();
							}
						}
						Deliveries d = new Deliveries();
						d.addDeliveries(deliID, empID, cusID, address, date);
						d.setPubName(publicationName);
						deliverList.add(d);
					}
					
				}
			}
		}

		return deliverList;
	}
	
	public boolean checkList(ArrayList<Deliveries> d)
	{
		if(d.size() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}


