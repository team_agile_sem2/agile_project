import junit.framework.TestCase;

public class CustomerTest extends TestCase 
{
	
	//Equivelance Partition for firstName
	//Test No:001
    //Test Objective: Invalid firstName length
    //Input(s): userName = Maciekkkkkkkkkkkk
    //Expected Output: false
	
    public void testCustomer001()
    {
        Customer testObject = new Customer();
        try
        {
        	boolean firstName = testObject.ValidateFirstName("Maciekkkkkkkkkkkk");
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid First Name must Start with capital and can only contains up to 16 characters", e.getMessage());
        }
    }
    
    //Test No:002
    //Test Objective: Valid firstName length, character and starts with capital
    //Input(s): userName = Maciek
    //Expected Output: false
	
    public void testCustomer002()
    {
        Customer testObject = new Customer();
    	try
        {
        	assertEquals(true, testObject.ValidateFirstName("Maciek"));
        }
        catch(addCustomerExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
  //Test No:003
    //Test Objective: Invalid firstName character
    //Input(s): userName = Maciek@
    //Expected Output: false
	
    public void testCustomer003()
    {
        Customer testObject = new Customer();
        
        try
        {
        	boolean firstName = testObject.ValidateFirstName("Maciek@");
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid First Name must Start with capital and can only contains up to 16 characters", e.getMessage());
        }
    }
    
    //Test No:004
    //Test Objective: Invalid firstName doesn't start with capital
    //Input(s): userName = maciek
    //Expected Output: false
	
    public void testCustomer004()
    {
        Customer testObject = new Customer();
        
        try
        {
        	boolean firstName = testObject.ValidateFirstName("maciek");
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid First Name must Start with capital and can only contains up to 16 characters", e.getMessage());
        }
    }
    
    //Equivelance Partition for lastName
    //Test No:005
    //Test Objective: Invalid lastName length
    //Input(s): lastName = karakkkkkkkkkkkkkk
    //Expected Output: false
	
    public void testCustomer005()
    {
        Customer testObject = new Customer();
        
        try
        {
        	boolean lastName = testObject.ValidateLastName("karakkkkkkkkkkkkkk");
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid Last Name/ can't be less 1 and greter than 16", e.getMessage());
        }
    }
    
    //Test No:006
    //Test Objective: Valid lastName 
    //Input(s): lastName = karak
    //Expected Output: true
	
    public void testCustomer006()
    {
        Customer testObject = new Customer();
        
        try
        {
        	assertEquals(true, testObject.ValidateLastName("karak"));
        }
        catch(addCustomerExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:007
    //Test Objective: Invalid lastName characters
    //Input(s): lastName = karak@
    //Expected Output: false
	
    public void testCustomer007()
    {
        Customer testObject = new Customer();
        
        try
        {
        	boolean lastName = testObject.ValidateLastName("karak@");
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid Last Name/ can't be less 1 and greter than 16", e.getMessage());
        }
    }
    
    //Test No:008
    //Test Objective: Valid lastName characters
    //Input(s):  lastName = kara'k
    //Expected Output: false
	
    public void testCustomer008()
    {
        Customer testObject = new Customer();
        
        try
        {
        	assertEquals(true, testObject.ValidateLastName("kara'k"));
        }
        catch(addCustomerExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:009
    //Test Objective: Invalid lastName start with characters
    //Input(s): lastName = 'karak
    //Expected Output: false
	
    public void testCustomer009()
    {
        Customer testObject = new Customer();
        
        try
        {
        	boolean lastName = testObject.ValidateLastName("'karak");
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid Last Name/ can't be less 1 and greter than 16", e.getMessage());
        }
    }
    
    //Equivalance Partition Test for Age
    //Test No:010
    //Test Objective: Invalid, Age is 5
    //Input(s): age = 5
    //Expected Output: addCustomerExceptionHandler
    
    public void testCustomer010()
    {
        Customer testObject = new Customer();
        try
        {
        	int age = testObject.ValidateAge(5);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid Age must be between 18 and 100", e.getMessage());
        }
    }
    
    //Test No:011
    //Test Objective: Valid, Age is 43
    //Input(s): age = 43
    //Expected Output: 43
    
    public void testCustomer011()
    {
        Customer testObject = new Customer();
        
        try
        {
        	assertEquals(43, testObject.ValidateAge(43));
        }
        catch(addCustomerExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:012
    //Test Objective: Invalid, Age is 152
    //Input(s): age = 152
    //Expected Output: addCustomerExceptionHandler
    
    public void testCustomer012()
    {
        Customer testObject = new Customer();
        try
        {
        	int age = testObject.ValidateAge(152);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid Age must be between 18 and 100", e.getMessage());
        }
    }
    
    //Boundry Test for Age
    //Test No:013
    //Test Objective: Invalid, Age is 17
    //Input(s): age = 17
    //Expected Output: addCustomerExceptionHandler
	
    public void testCustomer013()
    {
        Customer testObject = new Customer();
        try
        {
        	int age = testObject.ValidateAge(17);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid Age must be between 18 and 100", e.getMessage());
        }
    }
    
    //Test No:014
    //Test Objective: Valid, Age is 18
    //Input(s): age = 18
    //Expected Output: 18
	
    public void testCustomer014()
    {
        Customer testObject = new Customer();
        
        try
        {
        	assertEquals(18, testObject.ValidateAge(18));
        }
        catch(addCustomerExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:015
    //Test Objective: Valid, Age is 19
    //Input(s):  age = 19
    //Expected Output: 19
	
    public void testCustomer015()
    {
        Customer testObject = new Customer();
        
        try
        {
        	assertEquals(19, testObject.ValidateAge(19));
        }
        catch(addCustomerExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:016
    //Test Objective: Valid, Age is 99
    //Input(s): age = 99
    //Expected Output: 99
	
    public void testCustomer016()
    {
        Customer testObject = new Customer();
        
        try
        {
        	assertEquals(99, testObject.ValidateAge(99));
        }
        catch(addCustomerExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:017
    //Test Objective: Valid, Age is 100
    //Input(s): age = 100
    //Expected Output: addCustomerExceptionHandler
	
    public void testCustomer017()
    {
        Customer testObject = new Customer();
        
        try
        {
        	assertEquals(100, testObject.ValidateAge(100));
        }
        catch(addCustomerExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:018
    //Test Objective: Invalid, Age is 101
    //Input(s): age = 101
    //Expected Output: addCustomerExceptionHandler
	
    public void testCustomer018()
    {
        Customer testObject = new Customer();
        
        try
        {
        	int age = testObject.ValidateAge(101);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid Age must be between 18 and 100", e.getMessage());
        }
    }
    
    //Equivalence Partition for phone number
    //Test No:019
    //Test Objective: Invalid, Phone Number is too long
    //Input(s): phoneNumber = 08900000000
    //Expected Output: false
	
    public void testCustomer019()
    {
        Customer testObject = new Customer();
        
        try
        {
        	boolean phoneNumber = testObject.ValidatePhoneNumber("08900000000");
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid phone number must start with 0 and have 10 characters", e.getMessage());
        }
    }
    
    //Test No:020
    //Test Objective: Invalid, Phone Number is too short
    //Input(s): phoneNumber = 089000000
    //Expected Output: false
	
    public void testCustomer020()
    {
        Customer testObject = new Customer();
        try
        {
        	boolean phoneNumber = testObject.ValidatePhoneNumber("089000000");
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid phone number must start with 0 and have 10 characters", e.getMessage());
        }
    }
    
    //Test No:021
    //Test Objective: Invalid, Phone Number contains not a proper characters
    //Input(s): phoneNumber = 08a000000
    //Expected Output: false
	
    public void testCustomer021()
    {
        Customer testObject = new Customer();
        
        try
        {
        	boolean phoneNumber = testObject.ValidatePhoneNumber("08a000000");
        	fail("Should not get here ... Exception is expected");
        }
        catch(addCustomerExceptionHandler e)
        {
        	assertEquals("Invalid phone number must start with 0 and have 10 characters", e.getMessage());
        }
    }
    
    //Test No:022
    //Test Objective: Valid, Phone Number contains 10 character and only numbers
    //Input(s): phoneNumber = 0890000000
    //Expected Output: true
	
    public void testCustomer022()
    {
        Customer testObject = new Customer();
        
        try
        {
        	assertEquals(true, testObject.ValidatePhoneNumber("0890000000"));
        }
        catch(addCustomerExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Equivelance Partition for address
  	//Test No:0023
    //Test Objective: Invalid address1 length
    //Input(s): address1 = Tullamoreeeeeeeeeeeeeee
    //Expected Output: false
  	
      public void testCustomer023()
      {
          Customer testObject = new Customer();
          try
          {
          	boolean address1 = testObject.ValidateAddress1("Tullamoreeeeeeeeeeeeeee");
          	fail("Should not get here ... Exception is expected");
          }
          catch(addCustomerExceptionHandler e)
          {
          	assertEquals("Invalid Address1 must be between 1 and 20 characters", e.getMessage());
          }
      }
      
      //Test No:024
      //Test Objective: Valid address1 length
      //Input(s): address1 = Tullamore
      //Expected Output: true
  	
      public void testCustomer024()
      {
          Customer testObject = new Customer();
      	try
          {
          	assertEquals(true, testObject.ValidateAddress1("Tullamore"));
          }
          catch(addCustomerExceptionHandler e)
          {
          	fail("Should not get here ... Exception is expected");
          }
      }
      
    //Test No:025
      //Test Objective: Invalid address2 length
      //Input(s): address2 = dunfoteeeeeeeeeeeeeeeeeeeeeeee
      //Expected Output: false
  	
      public void testCustomer025()
      {
          Customer testObject = new Customer();
          
          try
          {
          	boolean address2 = testObject.ValidateAddress2("dunfoteeeeeeeeeeeeeeeeeeeeeeee");
          	fail("Should not get here ... Exception is expected");
          }
          catch(addCustomerExceptionHandler e)
          {
          	assertEquals("Invalid Address2 must be between 1 and 20 characters", e.getMessage());
          }
      }
      
      //Test No:026
      //Test Objective: Valid address2 length
      //Input(s): address2 = dunfote
      //Expected Output: false
  	
      public void testCustomer026()
      {
          Customer testObject = new Customer();
      	try
          {
          	assertEquals(true, testObject.ValidateAddress2("dunfote"));
          }
          catch(addCustomerExceptionHandler e)
          {
          	fail("Should not get here ... Exception is expected");
          }
      }
      
    //Test No:027
      //Test Objective: Invalid town length
      //Input(s): town = tulamoreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
      //Expected Output: false
  	
      public void testCustomer027()
      {
          Customer testObject = new Customer();
          try
          {
          	boolean town = testObject.ValidateTown("tulamoreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
          	fail("Should not get here ... Exception is expected");
          }
          catch(addCustomerExceptionHandler e)
          {
          	assertEquals("Invalid Town must start with capital and contain more than 1 and less than 17 characters", e.getMessage());
          }
      }
      
    //Test No:028
      //Test Objective: Invalid town, doesn't start with capital
      //Input(s): town = tulamor
      //Expected Output: false
  	
      public void testCustomer028()
      {
          Customer testObject = new Customer();
          try
          {
          	boolean town = testObject.ValidateTown("tulamor");
          	fail("Should not get here ... Exception is expected");
          }
          catch(addCustomerExceptionHandler e)
          {
          	assertEquals("Invalid Town must start with capital and contain more than 1 and less than 17 characters", e.getMessage());
          }
      }
          
    //Test No:029
      //Test Objective: Invalid town, contains illegal characters
      //Input(s): town = @tulamor
      //Expected Output: false
  	
      public void testCustomer029()
      {
          Customer testObject = new Customer();
          try
          {
          	boolean town = testObject.ValidateTown("@tulamor");
          	fail("Should not get here ... Exception is expected");
          }
          catch(addCustomerExceptionHandler e)
          {
          	assertEquals("Invalid Town must start with capital and contain more than 1 and less than 17 characters", e.getMessage());
          }
      }
              
      //Test No:030
      //Test Objective: Valid town, starts with capital and doesnt contains any illegal chracters
      //Input(s): town = Tullamore
      //Expected Output: false
  	
      public void testCustomer030()
      {
          Customer testObject = new Customer();
          try
          {
          	assertEquals(true, testObject.ValidateTown("Tullamore"));
          }
          catch(addCustomerExceptionHandler e)
          {
          	fail("Should not get here ... Exception is expected");
          }
      }
      
      //Test No:031
      //Test Objective: Invalid county length
      //Input(s): county = offalyllllllllllllllllllllllllllllllllll
      //Expected Output: false
  	
      public void testCustomer031()
      {
          Customer testObject = new Customer();
          try
          {
          	boolean county = testObject.ValidateCounty("offalylllllllllllllllllllllllllllllllllly");
          	fail("Should not get here ... Exception is expected");
          }
          catch(addCustomerExceptionHandler e)
          {
          	assertEquals("Invalid County must start with capital and contain more than 1 and less than 17 characters", e.getMessage());
          }
      }
          
    //Test No:032
      //Test Objective: Invalid county, doesnt start wiith capital
      //Input(s): county = offaly
      //Expected Output: false
  	
      public void testCustomer032()
      {
          Customer testObject = new Customer();
          try
          {
          	boolean county = testObject.ValidateCounty("offaly");
          	fail("Should not get here ... Exception is expected");
          }
          catch(addCustomerExceptionHandler e)
          {
          	assertEquals("Invalid County must start with capital and contain more than 1 and less than 17 characters", e.getMessage());
          }
      }
          
      //Test No:033
      //Test Objective: Invalid county, contains illegal caracters
      //Input(s): county = offaly*
      //Expected Output: false
  	
      public void testCustomer033()
      {
          Customer testObject = new Customer();
          try
          {
          	boolean county = testObject.ValidateCounty("offaly*");
          	fail("Should not get here ... Exception is expected");
          }
          catch(addCustomerExceptionHandler e)
          {
          	assertEquals("Invalid County must start with capital and contain more than 1 and less than 17 characters", e.getMessage());
          }
      }
          
     //Test No:034
     //Test Objective: Valid county, starts with capital and doesnt contains any illegal chracters
     //Input(s): county = Offaly
     //Expected Output: false
      	
      public void testCustomer034()
      {
          Customer testObject = new Customer();
          try
          {
          	assertEquals(true, testObject.ValidateCounty("Offaly"));
          }
          catch(addCustomerExceptionHandler e)
          {
          	fail("Should not get here ... Exception is expected");
          }
      }
      //Equivelance Partition for Deleting Customers
      //Test No:035
      //Test Objective: Customer didnt get deleted
      //Input(s): cusId = 99
      //Expected Output: false
  	
      public void testCustomer035()
      {
    	  
          CustomerController testObject = new CustomerController(null);
          Customer testObject2 = new Customer();
          testObject2.setCusId(99);
          try
          {
        	  assertEquals(false, testObject.deleteCustomer(testObject2));
          }
          catch(addCustomerExceptionHandler e)
          {
        	  fail("Should not get here ... Exception is expected");
          }
      }
      
      //Test No:036
      //Test Objective: Valid customer has been deleted
      //Input(s): cusId = 1000
      //Expected Output: true
  	
      public void testCustomer036() throws Exception
      {
          CustomerController testObject = new CustomerController(null);
          Customer testObject2 = new Customer();
          testObject2.setCusId(1000);
      	try
          {
          	assertEquals(true, testObject.deleteCustomer(testObject2));
          }
          catch(addCustomerExceptionHandler e)
          {
          	fail("Should not get here ... Exception is expected");
          }
      }
      
      //Equivelance Partition for ChanginCustomerData
      //Test No:037
      //Test Objective: Valid Address Change
      //Input(s): 1001,Durongatan Road, Dunvile House, Limerick, Leish
      //Expected Output: true  
      
      public void testCustomer037() throws Exception
      {
    	  CustomerController testObject = new CustomerController(null);
          Customer testObject2 = new Customer();
          testObject2.setCusId(1001);
          testObject2.setAddress1("Durongatan Road");
          testObject2.setAddress2("Dunvile House");
          testObject2.setTown("Limerick");
          testObject2.setCounty("Leish");
      	try
          {
          	assertEquals(true, testObject.changeCustomerAddress(testObject2));
          }
          catch(addCustomerExceptionHandler e)
          {
          	fail("Should not get here ... Exception is expected");
          }
      }
      
      //Test No:038
      //Test Objective: Invalid Address Change
      //Input(s): 99, Durongatan123321321312321312312 Road, Dunvile House, Limerick, Leish"
      //Expected Output: true  
      
      public void testCustomer038() throws Exception
      {
    	  CustomerController testObject = new CustomerController(null);
          Customer testObject2 = new Customer();
          testObject2.setCusId(99);
          testObject2.setAddress1("Durongatan123321321312321312312 Road");
          testObject2.setAddress2("Dunvile House");
          testObject2.setTown("Limerick");
          testObject2.setCounty("Leish");
          
          try
          {
        	  assertEquals(false, testObject.changeCustomerAddress(testObject2));
          }
          catch(Exception e)
          {
          	assertEquals(" ", e.getMessage());
          }
      }
      
      //Test No:039
      //Test Objective: Valid PhoneNumber Change
      //Input(s): 1001, 0894623374
      //Expected Output: true  
      
      public void testCustomer039() throws Exception
      {
          CustomerController testObject = new CustomerController(null);
          Customer cus = new Customer();
          cus.setCusId(1001);
          cus.setPhoneNumber("0894623374");
      	try
          {
          	assertEquals(true, testObject.changeCustomerPhoneNumber(cus));
          }
          catch(addCustomerExceptionHandler e)
          {
          	fail("Should not get here ... Exception is expected");
          }
      }
      
      //Test No:040
      //Test Objective: Invalid PhoneNumber Change
      //Input(s):089123123123
      //Expected Output: false  
      
      public void testCustomer040() throws Exception
      {
    	  CustomerController testObject = new CustomerController(null);
          Customer cus = new Customer();
          cus.setPhoneNumber("089123123123");
          
          try
          {
        	  assertEquals(false, testObject.changeCustomerPhoneNumber(cus));
          }
          catch(Exception e)
          {
        	  fail("Should not get here ... Exception is expected");
          }
      }
      
      //Equivelance Partition for ChanginCustomerStatus
      //Test No:041
      //Test Objective: Invalid Status Change
      //Input(s):99
      //Expected Output: false  
      
      public void testCustomer041() throws Exception
      {
    	  CustomerController testObject = new CustomerController(null);
          Customer cus = new Customer();
          cus.setCusId(99);
          
          try
          {
        	  assertEquals(false, testObject.changeCustomerStatus(cus));
          }
          catch(Exception e)
          {
        	  fail("Should not get here ... Exception is expected");
          }
      }
      
      //Test No:042
      //Test Objective: Valid Status Change
      //Input(s): 1001, active
      //Expected Output: true  
      
      public void testCustomer042() throws Exception
      {
          CustomerController testObject = new CustomerController(null);
          Customer cus = new Customer();
          cus.setCusId(1001);
          cus.setCustomerStatus("active");
      	try
          {
          	assertEquals(true, testObject.changeCustomerStatus(cus));
          }
          catch(addCustomerExceptionHandler e)
          {
          	fail("Should not get here ... Exception is expected");
          }
      }
}
