import junit.framework.TestCase;

public class publicationTest extends TestCase 
{
	
	//Equivelance Partition for publisherName
	//Test No:001
    //Test Objective: Invalid publisherName length
    //Input(s): publisherName = Patrykkkkkkkkkkkkkkkkk
    //Expected Output: false
	
    public void testName001()
    {
        publication testObject = new publication();
        try
        {
        	boolean publisherName = testObject.ValidatePublisherName("Patrykkkkkkkkkkkkkkkkk");
        	fail("Should not get here ... Exception is expected");
        }
        catch(addPublicationExceptionHandler e)
        {
        	assertEquals("Invalid publisher Name... Name has to contains up to 20 characters and contain at least 1 character", e.getMessage());
        }
    }
    
    //Test No:002
    //Test Objective: Valid publisherName length, characters and starts with capital
    //Input(s): publisherName = Patryk & Studio
    //Expected Output: true
	
    public void testName002()
    {
        publication testObject = new publication();
    	try
        {
        	assertEquals(true, testObject.ValidatePublisherName("Patryk & Studio"));
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:003
    //Test Objective: Valid publisherName length, characters and starts with capital
    //Input(s): publisherName = Patry 'k studio
    //Expected Output: true
	
    public void testName003()
    {
        publication testObject = new publication();
    	try
        {
        	assertEquals(true, testObject.ValidatePublisherName("Patry 'k studio"));
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:004
    //Test Objective: Valid publisherName length, characters and starts with capital
    //Input(s): publisherName = P.a.t.r.y.k
    //Expected Output: true
	
    public void testName004()
    {
        publication testObject = new publication();
    	try
        {
        	assertEquals(true, testObject.ValidatePublisherName("P.a.t.r.y.k"));
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Equivelance Partition for publicationName
    //Test No:005
    //Test Objective: Invalid publicationName length
    //Input(s): lastName = Irishhhhh timeeeeessss
    //Expected Output: false
	
    public void testName005()
    {
        publication testObject = new publication();
        
        try
        {
        	boolean publicationName = testObject.ValidatePublicationName("Irishhhhh timeeeeessss");
        	fail("Should not get here ... Exception is expected");
        }
        catch(addPublicationExceptionHandler e)
        {
        	assertEquals("Invalid Publication Name... Needs to contains up to 20 characters and at least 1 character", e.getMessage());
        }
    }
    
    //Test No:006
    //Test Objective: Valid publicationName 
    //Input(s): lastName = irish times
    //Expected Output: true
	
    public void testName006()
    {
        publication testObject = new publication();
        
        try
        {
        	assertEquals(true, testObject.ValidatePublicationName("irish times"));
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:007
    //Test Objective: Valid publicationName
    //Input(s): publicationName = @irish times@
    //Expected Output: true
	
    public void testName007()
    {
        publication testObject = new publication();
        
        try
        {
        	assertEquals(true, testObject.ValidatePublicationName("@irish & times@"));
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:008
    //Test Objective: Valid publicationName
    //Input(s):  lastName = irish 123 times
    //Expected Output: false
	
    public void testName008()
    {
        publication testObject = new publication();
        
        try
        {
        	assertEquals(true, testObject.ValidatePublicationName("irish 123 times"));
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Equivalance Partition Test for amount
    //Test No:009
    //Test Objective: Invalid amount
    //Input(s): amount = -22
    //Expected Output: addPublicationExceptionHandler
    
    public void testName009()
    {
        publication testObject = new publication();
        try
        {
        	int amount = testObject.ValidateAmount(-22);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addPublicationExceptionHandler e)
        {
        	assertEquals("Amount needs to be more than 0", e.getMessage());
        }
    }
    
    //Test No:010
    //Test Objective: Valid amount
    //Input(s): amount = 45
    //Expected Output: 45
    
    public void testName010()
    {
        publication testObject = new publication();
        
        try
        {
        	assertEquals(45, testObject.ValidateAmount(45));
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:011
    //Test Objective: Invalid amount
    //Input(s): amount = 0
    //Expected Output: addPublicationExceptionHandler
    
    public void testName011()
    {
        publication testObject = new publication();
        try
        {
        	int amount = testObject.ValidateAmount(0);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addPublicationExceptionHandler e)
        {
        	assertEquals("Amount needs to be more than 0", e.getMessage());
        }
    }
    
    //Boundry Test for amount
    //Test No:012
    //Test Objective: Invalid amount
    //Input(s): amount = -1
    //Expected Output: addPublicationExceptionHandler
	
    public void testName012()
    {
        publication testObject = new publication();
        try
        {
        	int amount = testObject.ValidateAmount(-1);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addPublicationExceptionHandler e)
        {
        	assertEquals("Amount needs to be more than 0", e.getMessage());
        }
    }
    
    //Test No:013
    //Test Objective: Valid amount
    //Input(s): amount = 1
    //Expected Output: 1
	
    public void testName013()
    {
        publication testObject = new publication();
        
        try
        {
        	assertEquals(1, testObject.ValidateAmount(1));
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:014
    //Test Objective: Invalid amount
    //Input(s):  amount = 0
    //Expected Output: addPublicationExceptionHandler
	
    public void testName014()
    {
        publication testObject = new publication();
        try
        {
        	int amount = testObject.ValidateAmount(0);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addPublicationExceptionHandler e)
        {
        	assertEquals("Amount needs to be more than 0", e.getMessage());
        }
    }
    //Equivalance Partition Test for price
    //Test No:015
    //Test Objective: Invalid price
    //Input(s): price = -22.00
    //Expected Output: addPublicationExceptionHandler
    
    public void testName015()
    {
        publication testObject = new publication();
        try
        {
        	float price = testObject.ValidatePrice((float) -22.00);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addPublicationExceptionHandler e)
        {
        	assertEquals("Price needs to be more than 0", e.getMessage());
        }
    }
    
    //Test No:016
    //Test Objective: Valid price
    //Input(s): price = 45.00
    //Expected Output: 45.00
    
    public void testName016()
    {
        publication testObject = new publication();
        
        try
        {
        	assertEquals(45.00, testObject.ValidatePrice((float) 45.00), 0);
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:017
    //Test Objective: Invalid price
    //Input(s): price = 0
    //Expected Output: addPublicationExceptionHandler
    
    public void testName017()
    {
        publication testObject = new publication();
        try
        {
        	float price = testObject.ValidatePrice(0);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addPublicationExceptionHandler e)
        {
        	assertEquals("Price needs to be more than 0", e.getMessage());
        }
    }
    
    //Boundry Test for price
    //Test No:018
    //Test Objective: Invalid price
    //Input(s): amount = -1.00
    //Expected Output: addPublicationExceptionHandler
	
    public void testName018()
    {
        publication testObject = new publication();
        try
        {
        	float price = testObject.ValidatePrice((float) -1.00);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addPublicationExceptionHandler e)
        {
        	assertEquals("Price needs to be more than 0", e.getMessage());
        }
    }
    
    //Test No:019
    //Test Objective: Valid price
    //Input(s): price = 1.00
    //Expected Output: 1.00
	
    public void testName019()
    {
        publication testObject = new publication();
        
        try
        {
        	assertEquals(1.00, testObject.ValidatePrice((float) 1.00), 0);
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:020
    //Test Objective: Invalid Price
    //Input(s):  price = 0.00
    //Expected Output: addPublicationExceptionHandler
	
    public void testName020()
    {
        publication testObject = new publication();
        try
        {
        	float price = testObject.ValidatePrice((float) 0.00);
        	fail("Should not get here ... Exception is expected");
        }
        catch(addPublicationExceptionHandler e)
        {
        	assertEquals("Price needs to be more than 0", e.getMessage());
        }
    }
    //Equivelance Partition for Deleting Publication
    //Test No:021
    //Test Objective: publication didn't get deleted
    //Input(s): cusId = 0
    //Expected Output: false
	
    public void testName021()
    {
  	  
        publicationController testObject = new publicationController();
        publication testObject2 = new publication();
        testObject2.setPub_id(0);
        try
        {
      	  assertEquals(false, testObject.deletePublication(testObject2));
        }
        catch(addPublicationExceptionHandler e)
        {
      	  fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:022
    //Test Objective: Valid publication has been deleted
    //Input(s): cusId = 2
    //Expected Output: true
	
    public void testName022() throws Exception
    {
    	publicationController testObject = new publicationController();
    	publication testObject2 = new publication();
        testObject2.setPub_id(2);
    	try
        {
        	assertEquals(true, testObject.deletePublication(testObject2));
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Equivelance Partition for ChangingPublication
    //Test No:023
    //Test Objective: Valid publication amount change
    //Input(s): 14, 50
    //Expected Output: true  
    
    public void testName023() throws Exception
    {
    	publicationController testObject = new publicationController();
    	publication testObject2 = new publication();
        testObject2.setPub_id(14);
        testObject2.setAmount(50);
        try
        {
        	assertEquals(true, testObject.changePublication(testObject2));
        }
        catch(addPublicationExceptionHandler e)
        {
        	fail("Should not get here ... Exception is expected");
        }
    }
    
    //Test No:024
    //Test Objective: Invalid publication amount Change
    //Input(s): 99, Durongatan123321321312321312312 Road, Dunvile House, Limerick, Leish"
    //Expected Output: true  
    
    public void testName024() throws Exception
    {
    	publicationController testObject = new publicationController();
    	publication testObject2 = new publication();
        testObject2.setPub_id(0);
        testObject2.setAmount(0);
        try
        {
      	  assertEquals(false, testObject.changePublication(testObject2));
        }
        catch(Exception e)
        {
        	assertEquals(" ", e.getMessage());
        }
    }

}