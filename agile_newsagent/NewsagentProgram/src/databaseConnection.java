import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class databaseConnection
{
	static Connection con = null;
	static Statement stmt = null;

	public static void cleanup_resources()
	{
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}
	public static void init_db()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url="jdbc:mysql://localhost:3307/Agile_Database";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
	
	public static void addAccount(Account acc)
	{
		try
		{
			String str ="INSERT INTO login_info VALUES(null,?,?)";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setString(1,acc.getUser());
			pstmt.setString(2,acc.getPass());
			pstmt.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println("Error adding account!");
			e.printStackTrace();
		}
		
	}
	
	public static void addCustomer(Customer cus) throws addCustomerExceptionHandler
	{
		try
		{
			String str = "INSERT INTO customer VALUES(null,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setString(1,cus.getFirstName());
			pstmt.setString(2,cus.getLastName());
			pstmt.setString(3,cus.getAddress1());
			pstmt.setString(4,cus.getAddress2());
			pstmt.setString(5,cus.getTown());
			pstmt.setString(6,cus.getCounty());
			pstmt.setInt(7,cus.getAge());
			pstmt.setString(8,cus.getPhoneNumber());
			pstmt.setString(9,cus.getCustomerStatus());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error adding Customer");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}
	}
	
	public static void updateCustomerStatus(Customer cus) throws addCustomerExceptionHandler
	{
		try
		{
			String str = "UPDATE Customer SET cus_status = ? WHERE cus_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setInt(2, cus.getCusId());
			pstmt.setString(1, cus.getCustomerStatus());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error changing Status");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}
	}
	
	public static void DeleteCustomer(Customer cus)
	{
		try
		{
			String str = "DELETE FROM Subscriptions WHERE cus_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setInt(1,cus.getCusId());
			
			int num = pstmt.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println("Error deleting Customer");
			System.out.println(e.getMessage());
			System.out.println(stmt);
		}
		
		try
		{
			String str = "DELETE FROM Customer WHERE cus_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setInt(1,cus.getCusId());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error deleting Customer");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}
	}
	
	public static void UpdateCustomerAddress(Customer cus)
	{
		try
		{
			String str = "UPDATE Customer SET address1 = ?, address2 = ?, town = ?, county = ? WHERE cus_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setString(1, cus.getAddress1());
			pstmt.setString(2, cus.getAddress2());
			pstmt.setString(3, cus.getTown());
			pstmt.setString(4, cus.getCounty());
			pstmt.setInt(5, cus.getCusId());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error Changing The Address");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}
	}
	
	public static void UpdateCustomerPhoneNumber(Customer cus)
	{
		try
		{
			String str = "UPDATE Customer SET phone_no = ? WHERE cus_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setInt(2, cus.getCusId());
			pstmt.setString(1, cus.getPhoneNumber());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error changing phone Number");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}
		
	}
	
	public ArrayList<Customer> getCustomers()
	{
		ArrayList<Customer> cusList = new ArrayList<Customer>();
		
		try
		{
			ResultSet rs = stmt.executeQuery("Select * FROM Customer");
			
			while(rs.next())
			{
				int cusid = rs.getInt(1);
				String fName = rs.getString(2);
				String lName = rs.getString(3);
				String address1 = rs.getString(4);
				String address2 = rs.getString(5);
				String town = rs.getString(6);
				String county = rs.getString(7);
				int age = rs.getInt(8);
				String phoneNum = rs.getString(9);
				String cusStatus = rs.getString(10);
				
				Customer cus = new Customer(cusid, fName, lName, address1, address2, town, county, age, phoneNum, cusStatus);
				cusList.add(cus);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println("Error getting Customers");
			System.out.println(sqle.getMessage());
		}
	
		return cusList;
	}
	
	public static boolean addSubscription(Customer cus, publication pub, Subscription sub)throws  SQLException
	{
		try
		{
			String str = "INSERT INTO Subscriptions VALUES(?,?,?);";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setInt(1, pub.getPub_id());
			pstmt.setInt(2, cus.getCusId());
			pstmt.setString(3, sub.getFrequency());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("Error Adding Subscription");
			System.out.println(e.getMessage());
			System.out.println(stmt);
			return false;
		}
		return true;
	}
	public ArrayList<Subscription> getSubscriptionPrint()
	{
		ArrayList<Subscription> subCusIDs = new ArrayList<Subscription>();
		
		try
		{
			ResultSet rs = stmt.executeQuery("select distinct t1.cus_id, t1.first_name, t2.pub_id, t2.publication_name, t3.frequency from Customer as t1, Publication as t2, Subscriptions as t3  where t1.cus_id = t3.cus_id and t2.pub_id = t3.pub_id order by t3.cus_id ASC;");
			
			while(rs.next())
			{
				int customerID = rs.getInt(1);
				String cusName = rs.getString(2);
				int publiID = rs.getInt(3);
				String publiName = rs.getString(4);
				String freq = rs.getString(5);
				Subscription sub = new Subscription(customerID, cusName, publiID, publiName, freq);
				subCusIDs.add(sub);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println("Error getting cus_id from subscription");
			System.out.println(sqle.getMessage());
		}
		
		return subCusIDs;
	}
	
	
	
	public ArrayList<Integer> getCustomerID()
	{
		ArrayList<Integer> cusIDs = new ArrayList<Integer>();
		
		try
		{
			ResultSet rs = stmt.executeQuery("Select cus_id FROM Customer");
			
			while(rs.next())
			{
				int customerID = rs.getInt(1);
				cusIDs.add(customerID);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println("Error getting cus_id");
			System.out.println(sqle.getMessage());
		}
		
		return cusIDs;
	}
	//Make just make a String[]
	public ArrayList<String> getUserName()
	{
		ArrayList<String> user = new ArrayList<String>();
		
		try
		{
			ResultSet rs = stmt.executeQuery("Select username FROM login_info");
			
			while(rs.next())
			{
				String username = rs.getString(1);
				user.add(username);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println("Error getting login info!");
			System.out.println(sqle.getMessage());
		}
		
		return user;
	}
	
	public ArrayList<String> getPassword()
	{
		ArrayList<String> pass = new ArrayList<String>();
		
		try
		{
			ResultSet rs = stmt.executeQuery("Select password FROM login_info");
			
			while(rs.next())
			{
				String password = rs.getString(1);
				pass.add(password);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println("Error getting login info!");
			System.out.println(sqle.getMessage());
		}
		
		return pass;
	}
	
	//publication
	public static void addPublication(publication pub) throws addPublicationExceptionHandler
	{
		try
		{
			String str = "INSERT INTO publication VALUES(null,?,?,?,?)";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setString(1,pub.getPublicationName());
			pstmt.setString(2,pub.getPublisherName());
			pstmt.setFloat(3,pub.getPrice());
			pstmt.setInt(4,pub.getAmount());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error adding Publication");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}
	}
	public static void deletePublication(publication pub)
	{
		try
		{
			String str = "DELETE FROM Subscriptions WHERE pub_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setInt(1,pub.getPub_id());
			
			int num = pstmt.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println("Error deleting Customer");
			System.out.println(e.getMessage());
			System.out.println(stmt);
		}
		
		try
		{
			String str = "DELETE FROM Publication WHERE pub_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setInt(1,pub.getPub_id());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error deleting publication");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}
	}
	
	public ArrayList<publication> getPublications()
	{
		ArrayList<publication> pubList = new ArrayList<publication>();
		
		try
		{
			ResultSet rs = stmt.executeQuery("Select * FROM Publication");
			
			while(rs.next())
			{
				int pubid = rs.getInt(1);
				String PublicationName = rs.getString(2);
				String PublisherName = rs.getString(3);
				float Price = rs.getFloat(4);
				int Amount = rs.getInt(5);
				
				publication pub = new publication(pubid, PublicationName, PublisherName,Price, Amount);
				pubList.add(pub);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println("Error getting Publication");
			System.out.println(sqle.getMessage());
		}	
		return pubList;
	}
	
	public ArrayList<Integer> getPublicationID()
	{
		ArrayList<Integer> pubID = new ArrayList<Integer>();
		
		try
		{
			ResultSet rs = stmt.executeQuery("Select pub_id FROM Publication");
			
			while(rs.next())
			{
				int publicationID = rs.getInt(1);
				pubID.add(publicationID);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println("Error getting publication ID!");
			System.out.println(sqle.getMessage());
		}
		
		return pubID;
	}
	
	public static void UpdatePublicationAmount(publication pub)
	{
		try
		{
			String str = "UPDATE Publication SET amount = ? WHERE pub_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setInt(1, pub.getAmount());
			pstmt.setInt(2, pub.getPub_id());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error changing amount");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}
		
	}
	
	public static void addEmployee(Employee emp) throws EmployeeExceptionHandler
	{
		try
		{
			String str = "INSERT INTO employee VALUES(null,?,?,?,?,?)";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setString(1,emp.getFirst_name());
			pstmt.setString(2,emp.getLast_name());
			pstmt.setString(3,emp.getAddress());
			pstmt.setString(4,emp.getPhone_no());
			pstmt.setInt(5,emp.getAge());
			
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error adding Employee");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}
		System.out.println("Employee added Succesfully");
	}
	
	public static void DeleteEmployee(Employee emp)
	{
		try
		{
			String str = "DELETE FROM Employee WHERE emp_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setInt(1,emp.getEmpId());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error deleting Employee");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}
	}
	
	public ArrayList<Employee> getEmployee()
	{
		ArrayList<Employee> empList = new ArrayList<Employee>();
		
		try
		{
			ResultSet rs = stmt.executeQuery("Select * FROM Employee");
			
			while(rs.next())
			{
				int empid = rs.getInt(1);
				String fName = rs.getString(2);
				String lName = rs.getString(3);
				String address = rs.getString(4);
				String phoneNum = rs.getString(6);
				int age = rs.getInt(5);
				
				Employee emp = new Employee(empid, fName, lName, address, age, phoneNum);
				empList.add(emp);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println("Error getting Employees");
			System.out.println(sqle.getMessage());
		}
	
		return empList;
	}
	public ArrayList<Integer> getEmployeeID()
	{
		ArrayList<Integer> empIDs = new ArrayList<Integer>();
		
		try
		{
			ResultSet rs = stmt.executeQuery("Select emp_id FROM Employee");
			
			while(rs.next())
			{
				int employeeID = rs.getInt(1);
				empIDs.add(employeeID);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println("Error getting emp_id");
			System.out.println(sqle.getMessage());
		}
		
		return empIDs;
	}
	
	public static void UpdateEmployeeAddress(Employee emp)
	{
		try
		{
			String str = "UPDATE Employee SET address = ? WHERE emp_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setString(1, emp.getAddress());
			pstmt.setInt(2, emp.getEmpId());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error Changing The Address");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}
	}
	
	public static void UpdateEmployeePhoneNumber(Employee emp)
	{
		try
		{
			String str = "UPDATE Employee SET phone_no = ? WHERE emp_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setString(1, emp.getPhone_no());
			pstmt.setInt(2, emp.getEmpId());
			
			int num = pstmt.executeUpdate();
		}
		catch(SQLException sqle)
		{
			System.out.println("Error changing phone Number");
			System.out.println(sqle.getMessage());
			System.out.println(stmt);
		}

		System.out.println("PhoneNumber Succesfully Changed");
		System.out.println("  ");
		
	}
	
	public ArrayList<Deliveries> getDeliveries()
	{
		ArrayList<Deliveries> Dels = new ArrayList<Deliveries>();
		
		try
		{
			String getInfo ="select * from delivery";
			ResultSet rs = stmt.executeQuery(getInfo);
			while(rs.next())
			{
				int deliID = rs.getInt(1);
				int employeeID = rs.getInt(2);
				int customerID = rs.getInt(3);
				String date = rs.getString(4);

				Deliveries del = new Deliveries();
				del.addDeliveries(deliID,employeeID,customerID," ",date);
				Dels.add(del);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println(sqle.getMessage());
		}
		return Dels;
	}
	
	public ArrayList<DeliContent> getDeliContent()
	{
		ArrayList<DeliContent> deli = new ArrayList<DeliContent>();
		try
		{
			String getDeli = "select * from deli_to";
			ResultSet rs = stmt.executeQuery(getDeli);
			while(rs.next())
			{
				int deliID = rs.getInt(1);
				int pubID = rs.getInt(2);
	
				DeliContent del = new DeliContent(deliID,pubID);
				
				deli.add(del);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println(sqle.getMessage());
		}
		return deli;
	}
}
