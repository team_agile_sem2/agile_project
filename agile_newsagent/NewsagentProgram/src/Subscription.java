
public class Subscription
{
	private int cusID;
	private String cusName;
	private int pubID;
	private String pubName;
	private  String frequency;
	
	public Subscription() {
		
	}
	
	public Subscription(int cusID, String cusName, int pubID, String pubName, String frequency) {
		
		this.cusID = cusID;
		this.cusName = cusName;
		this.pubID = pubID;
		this.pubName = pubName;
		this.frequency = frequency;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
	public String toString()
	{
		return "Customer ID: " + cusID + "\nCustomer Name: " + cusName + "\nPublication ID: " + pubID + "\nPublication Name: " + pubName + "\nFrequency: " + frequency + "\n";
	}
}
