DROP DATABASE IF EXISTS Agile_Database;
CREATE DATABASE IF NOT EXISTS Agile_Database;
USE Agile_Database;

/*Customers Table*/
DROP TABLE IF EXISTS Customer;

CREATE TABLE Customer (
	cus_id INTEGER AUTO_INCREMENT ,
	first_name VARCHAR(16) NOT NULL,
	last_name VARCHAR(16) NOT NULL,
	address1 VARCHAR(20) NOT NULL,
    address2 VARCHAR(20) NOT NULL,
    town VARCHAR(16) NOT NULL,
    county VARCHAR(16) NOT NULL,
	age INT NOT NULL,
	phone_no VARCHAR(12) NOT NULL,
    cus_status VARCHAR(12) NOT NULL,
	PRIMARY KEY(cus_id) ); 
alter table Customer AUTO_INCREMENT = 1000;

INSERT INTO Customer VALUES (null,"Mazzik","Mann", "dun", "home", "tull", "off", 18, "0890002340", "holiday");
INSERT INTO Customer VALUES (null,"Mazik","Man", "dunf", "homes", "tulla", "offa", 21, "0890002340", "active");

SELECT 'INSERTING DATA INTO TABLE CUSTOMER' as 'INFO';



SELECT * FROM Customer;

/*Employee Table*/
DROP TABLE IF EXISTS Employee;

CREATE TABLE Employee (
	emp_id INTEGER AUTO_INCREMENT,
	first_name VARCHAR(16) NOT NULL,
	last_name VARCHAR(16) NOT NULL,
	address VARCHAR(16) NOT NULL,
	age INTEGER NOT NULL,
	phone_no INTEGER NOT NULL,
	PRIMARY KEY(emp_id));

INSERT INTO Employee VALUES (null,"Darragh","Friedclarken","Ath", 19, "0890000012");

SELECT 'INSERTING DATA INTO TABLE EMPLOYEE' as 'INFO';



SELECT * FROM Employee;
/*Publications Table*/
DROP TABLE IF EXISTS Publication;

CREATE TABLE Publication (
	pub_id INTEGER AUTO_INCREMENT,
	publication_name VARCHAR(20) NOT NULL,
	publisher_name VARCHAR(20) NOT NULL,
	price FLOAT NOT NULL,
	amount INTEGER NOT NULL,
	PRIMARY KEY(pub_id));
	
INSERT INTO Publication  VALUES (null,"Patryk","Honolulu",55, 20);

SELECT 'INSERTING DATA INTO TABLE PUBLICATION' as 'INFO';



SELECT * FROM Publication;
/*Deliveries Table*/
DROP TABLE IF EXISTS Delivery;

CREATE TABLE Delivery (
	deli_id INTEGER AUTO_INCREMENT,
	emp_id INTEGER NOT NULL,
	cus_id INTEGER NOT NULL,
	date_to_deliver DATE NOT NULL,
	PRIMARY KEY(deli_id, emp_id, cus_id),
	FOREIGN KEY (cus_id) REFERENCES Customer (cus_id),
	FOREIGN KEY (emp_id) REFERENCES Employee (emp_id));

/*Invoice Table*/
DROP TABLE IF EXISTS Invoice;

CREATE TABLE Invoice (
	inv_id INTEGER NOT NULL,
	cus_id INTEGER NOT NULL ,
	deli_id INTEGER NOT NULL,
	isue_date INTEGER NOT NULL,
	PRIMARY KEY(inv_id, cus_id, deli_id),
	FOREIGN KEY (cus_id) REFERENCES Customer (cus_id),
	FOREIGN KEY (deli_id) REFERENCES Delivery (deli_id));

/*Subscribes_to Table*/
DROP TABLE IF EXISTS Subscriptions;

CREATE TABLE Subscriptions (
	pub_id INTEGER NOT NULL,
	cus_id INTEGER NOT NULL,
	frequency VARCHAR(10) NOT NULL,
	PRIMARY KEY(cus_id, pub_id),
	FOREIGN KEY (cus_id) REFERENCES Customer (cus_id),
	FOREIGN KEY (pub_id) REFERENCES Publication (pub_id));
	
insert into subscriptions values ( (select pub_id from publication where pub_id = 1), (select cus_id from customer where cus_id = 1000),"daily");	
/*deli_to Table*/
DROP TABLE IF EXISTS deli_to;

CREATE TABLE deli_to (
	deli_id INTEGER NOT NULL,
	pub_id INTEGER NOT NULL,
	PRIMARY KEY(deli_id, pub_id),
	FOREIGN KEY (deli_id) REFERENCES Delivery (deli_id),
	FOREIGN KEY (pub_id) REFERENCES Publication (pub_id));

/*user_info Table*/
DROP TABLE IF EXISTS login_info;

CREATE TABLE login_info (
log_id INTEGER AUTO_INCREMENT,
username VARCHAR(15) NOT NULL,
password VARCHAR(20) NOT NULL,
PRIMARY KEY(log_id));

INSERT INTO login_info VALUES (null,"admin","Admin69");


DELIMITER 
CREATE PROCEDURE addDeliveries()
BEGIN
		DECLARE cusID int;
        DECLARE empID int;
		DECLARE finished int default 0;
		
		DECLARE cur1 CURSOR FOR
			select distinct(cus_id)
			from Subscriptions 
			where frequency like 'daily' or frequency like dayname(now());
            
		DECLARE emp1 CURSOR FOR
			select distinct(emp_id)
			from Employee
			where emp_id = 1;
		
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
		
		OPEN cur1;
        OPEN emp1;
        
		build_delivery_table: LOOP
			fetch cur1 into cusID;
			fetch emp1 into empID;
            
			IF finished = 1 then 
				leave build_delivery_table;
			END IF;
			
			insert into Delivery  values(null, empID, cusID, now());
			
		END LOOP;
END 


DELIMITER $$
CREATE PROCEDURE addDeliversContents()
BEGIN
	DECLARE cusId, pubId, delId int;
	DECLARE finished int default 0;
	
	DECLARE cur1 CURSOR FOR
		select cus_id, pub_id
		from Subscriptions
		where frequency like 'daily' or frequency like dayname(now());
		
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET	finished = 1;
	
	OPEN  cur1;
	
	build_deliveryContents_table: LOOP
	
		fetch cur1 into cusId, pubId;
		
		IF finished = 1 THEN
			leave build_deliveryContents_table;
		END IF;

		select deli_id into delId
		from Delivery
		where cus_id = cusId and datediff(date_to_deliver, now()) = 0;
		
		insert into deli_to values(delId, pubId);
		
	END LOOP;
			
END $$